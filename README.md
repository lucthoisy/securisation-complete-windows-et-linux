# Sécurisation Complète Windows et Linux
L'objectif est d'empêcher l'ordinateur de booter sur Windows ou une distribution Linux sans qu'un mot de passe n'ait été saisi.
Le mot de passe est stockée sur une clé Yubikey.


- DcsProp: fichier de configuration du bootloader de veracrypt ("KeyboardInputDelay" permet de régler de délais entre chaque frappe du clavier)
- yubikey_configuration.csv: fichier de configuration de la clé Yubikey (configurée avec le logiciel Yubikey Personalization Tool)
